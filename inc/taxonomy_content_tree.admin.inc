<?php

/**
 * @file
 * Administrative page callbacks for the AKS taxonomy content tree module.
 */

/**
 * Form builder to set taxonomy content tree blocks.
 */
function taxonomy_content_tree($form) {
  // get all entity bundels with term reference
  $aks_excluded_entity = array(
    'comment',
    'file',
    'taxonomy_term',
    'taxonomy_vocabulary',
    'user'
  );
  $aks_entity_bundles = array();
  $aks_entities = entity_get_info();
  foreach($aks_entities as $entity => $bundle_ary) {
  	if(!in_array($entity, $aks_excluded_entity)) {
  	  foreach($bundle_ary['bundles'] as $bundle_name => $bundle) {
  	  	$instances = field_info_instances($entity, $bundle_name);
  	  	foreach($instances as $fld_name => $flds) {
          if( $flds['display']['default']['type'] == 'taxonomy_term_reference_link') {
          	$aks_entity_bundles[$entity][$bundle_name][$fld_name] = $flds['label'];
            continue;
          }
        }
  	  }
    }
  }
  //$extra_fields = field_info_extra_fields('node', 'badge', 'form');
  
  // build configuration form
  foreach($aks_entity_bundles as $entity_type => $bundles) {
  	$aks_entity_type = str_replace('_', '-', $entity_type);
    $form["aks_$entity_type"] = array(
      '#type'=> 'fieldset',
      '#title'=> t($entity_type),
      '#collapsible'=>true,
      '#collapsed'=>FALSE,
    );
    foreach($bundles as $bundle_name => $flds) {
      $aks_bundle_name = str_replace('_', '-', $bundle_name);
      if(count($flds) == 1) {
        foreach($flds as $fld_name => $fld_label) {
      	  $fld_name = str_replace('_', '-', $fld_name);
        }
      	$variable_name = 'aks_tct_' . $aks_entity_type . '_' . $aks_bundle_name . '_' . $fld_name;
      	if(variable_get($variable_name,0)) {
      	  $form["aks_$entity_type"][$variable_name] = array(
            '#type'=> 'checkbox',
            '#title'=> t("Build tree block for <b>$bundle_name</b>"),
            '#attributes' => array('checked' => 'checked'),
          );
        } else {
          $form["aks_$entity_type"][$variable_name] = array(
            '#type'=> 'checkbox',
            '#title'=> t("Build tree block for <b>$bundle_name</b>"),
          );
        }
      } else {
      	$form["aks_$entity_type"]["aks_$bundle_name"] = array(
          '#type'=> 'fieldset',
          '#title'=> t($bundle_name),
          '#collapsible'=>true,
          '#collapsed'=>FALSE,
        );
      	foreach($flds as $fld_name => $fld_label) {
      	  $aks_fld_name = str_replace('_', '-', $fld_name);
          $variable_name = 'aks_tct_' . $aks_entity_type . '_' . $aks_bundle_name . '_' . $aks_fld_name;
          if(variable_get($variable_name)) {
      	    $form["aks_$entity_type"]["aks_$bundle_name"][$variable_name] = array(
              '#type'=> 'checkbox',
              '#title'=> t("Build tree block  on <b>$fld_label</b> ($fld_name)"),
              '#attributes' => array('checked' => 'checked'),
            );
          } else {
            $form["aks_$entity_type"]["aks_$bundle_name"][$variable_name] = array(
              '#type'=> 'checkbox',
              '#title'=> t("Build tree block on <b>$fld_label</b> ($fld_name)"),
            );
          }
      	}
      }
    }
  }
  return system_settings_form($form);
}